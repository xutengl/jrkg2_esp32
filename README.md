| Supported Targets | ESP32 | ESP32-C2 | ESP32-C3 | ESP32-S2 | ESP32-S3 |
| ----------------- | ----- | -------- | -------- | -------- | -------- |

# JrkG2
imported from JrkG2 Arduino library, this library replaced out the stream class with ESP_IDF uart class to run JrkG2 examples natively on Esp32.Enjoy~

```
├── CMakeLists.txt
├── main
│   ├── CMakeLists.txt
|   ├── jrkg2_base.h
|   ├── jrkg2_serial.h
|   ├── jrkg2_serial.cpp
│   └── main.cpp
|   
└── README.md                  This is the file you are currently reading
```

