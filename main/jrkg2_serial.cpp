#include <jrkg2_serial.h>

#ifdef __cplusplus
extern "C" {
#endif

/**** JrkG2Serial ****/

void JrkG2Serial::commandW7(uint8_t cmd, uint8_t val)
{
  sendCommandHeader(cmd);
  serialW7(val);

  _lastError = 0;
}

void JrkG2Serial::commandWs14(uint8_t cmd, int16_t val)
{
  uint16_t v = val;
  sendCommandHeader(cmd);
  serialW7(v);  // lower 7 bits
  serialW7(v >> 7);  // upper 7 bits

  _lastError = 0;
}

uint8_t JrkG2Serial::commandR8(uint8_t cmd)
{
  uint8_t val;

  sendCommandHeader(cmd);

  uint8_t byteCount = uart_read_bytes(_uart_num, &val, 1, 500/portTICK_PERIOD_MS);
  //_stream->readBytes(&val, 1);
  if (byteCount != 1)
  {
    _lastError = JrkG2CommReadError;
    return 0;
  }

  _lastError = 0;
  return val;
}

uint16_t JrkG2Serial::commandR16(uint8_t cmd)
{
  uint8_t buffer[2];

  sendCommandHeader(cmd);

  uint8_t byteCount = uart_read_bytes(_uart_num, buffer, 2, 500/portTICK_PERIOD_MS);
  //_stream->readBytes(buffer, 2);
  if (byteCount != 2)
  {
    _lastError = JrkG2CommReadError;
    return 0;
  }

  _lastError = 0;
  return ((uint16_t)buffer[0] << 0) | ((uint16_t)buffer[1] << 8);
}

void JrkG2Serial::segmentRead(uint8_t cmd, uint8_t offset,
  uint8_t length, uint8_t * buffer)
{
  // The Jrk does not allow reads longer than 15 bytes.
  if (length > 15) { length = 15; }

  sendCommandHeader(cmd);
  serialW7(offset);
  serialW7(length);

  uint8_t byteCount = uart_read_bytes(_uart_num, buffer, length, 500/portTICK_PERIOD_MS);
  //_stream->readBytes(buffer, length);
  
  if (byteCount != length)
  {
    _lastError = JrkG2CommReadError;

    // Set the buffer bytes to 0 so the program will not use an uninitialized
    // variable.
    memset(buffer, 0, length);
    return;
  }

  _lastError = 0;
}

void JrkG2Serial::segmentWrite(uint8_t cmd, uint8_t offset,
  uint8_t length, uint8_t * buffer)
{
  // The Jrk does not accept writes longer than 7 bytes over serial.
  if (length > 7) { length = 7; }

  sendCommandHeader(cmd);
  serialW7(offset);
  serialW7(length);

  // bit i = most-significant bit of buffer[i]
  uint8_t msbs = 0;
  for (uint8_t i = 0; i < length; i++)
  {
    serialW7(buffer[i]);
    msbs |= (buffer[i] >> 7 & 1) << i;
  }
  serialW7(msbs);

  _lastError = 0;
}

void JrkG2Serial::sendCommandHeader(uint8_t cmd)
{
  if (_deviceNumber == 255)
  {
    // Compact protocol
    // _stream->write((uint8_t)cmd);
    uart_write_bytes(_uart_num, &cmd, sizeof(uint8_t));
  }
  else
  {
    // Pololu protocol
    // _stream->write(0xAA);
    uint8_t header = 0xAA;
    uart_write_bytes(_uart_num, &header, sizeof(uint8_t));
    serialW7(_deviceNumber);
    serialW7((uint8_t)cmd);
  }
  _lastError = 0;
}

void JrkG2Serial::serialW7(uint8_t val) 
{ 
  uint8_t valTemp = (uint8_t)(val & 0x7F);
  uart_write_bytes(_uart_num, &valTemp, sizeof(uint8_t));
    // _stream->write(val & 0x7F); 
}


#ifdef __cplusplus
}
#endif