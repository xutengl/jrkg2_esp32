
#pragma once

#include "jrkg2_base.h"
#include "string.h"
#include "driver/uart.h"
#include "freertos/FreeRTOS.h"

#ifdef __cplusplus
extern "C" {
#endif


/// Represents a serial connection to a Jrk G2.
///
/// For the high-level commands you can use on this object, see JrkG2Base.
class JrkG2Serial : public JrkG2Base
{
public:
  /// Creates a new JrkG2Serial object.
  JrkG2Serial(uart_port_t uart_num, uint8_t deviceNumber = 255) :
    _uart_num(uart_num),
    _deviceNumber(deviceNumber)
  {
  }

  /// Gets the serial device number this object is using.
  uint8_t getDeviceNumber() { return _deviceNumber; }

private:
  uart_port_t  _uart_num;
  const uint8_t _deviceNumber;

  void commandQuick(uint8_t cmd) { sendCommandHeader(cmd); }
  void commandW7(uint8_t cmd, uint8_t val);
  void commandWs14(uint8_t cmd, int16_t val);
  uint8_t commandR8(uint8_t cmd);
  uint16_t commandR16(uint8_t cmd);
  void segmentRead(uint8_t cmd, uint8_t offset,
    uint8_t length, uint8_t * buffer);
  void segmentWrite(uint8_t cmd, uint8_t offset,
    uint8_t length, uint8_t * buffer);

  void sendCommandHeader(uint8_t cmd);
  void serialW7(uint8_t val);
};


#ifdef __cplusplus
}
#endif