#include <stdio.h>
#include "jrkg2_serial.h"
#include "freertos/FreeRTOS.h"

void uartConfig();

extern "C" void app_main(void)
{
    uartConfig();
    JrkG2Serial motorLeft(UART_NUM_1, 1);
    JrkG2Serial motorRight(UART_NUM_1, 2);
    int targetL=0, targetR=0;

    while(true){
        targetL = 0xFFF/2 + 500;
        targetR = 0xFFF/2 - 500;
        motorLeft.setTarget(targetL);
        motorRight.setTarget(targetR);
        printf("left: %d, right: %d\n", targetL, targetR);
        vTaskDelay(1000 / portTICK_PERIOD_MS);

        targetL = 0xFFF/2 - 500;
        targetR = 0xFFF/2 + 500;
        motorLeft.setTarget(targetL);
        motorRight.setTarget(targetR);
        printf("left: %d, right: %d\n", targetL, targetR);
        vTaskDelay(1000 / portTICK_PERIOD_MS);
    }
}

void uartConfig()
{
#define TXD_PIN 4
#define RXD_PIN 5

#define RX_BUF_SIZE 1024
    uart_config_t uart_config = {
        .baud_rate = 9600,
        .data_bits = UART_DATA_8_BITS,
        .parity = UART_PARITY_DISABLE,
        .stop_bits = UART_STOP_BITS_1,
        .flow_ctrl = UART_HW_FLOWCTRL_DISABLE,
        .source_clk = UART_SCLK_DEFAULT};

    uart_param_config(UART_NUM_1, &uart_config);
    uart_set_pin(UART_NUM_1, TXD_PIN, RXD_PIN, UART_PIN_NO_CHANGE, UART_PIN_NO_CHANGE);
    uart_driver_install(UART_NUM_1, RX_BUF_SIZE, 0, 0, NULL, 0);
}
